<?php
define( "ESCAPE_MODE_DOUBLE", 1 );
define( "ESCAPE_MODE_SINGLE", 2 );

class Util {
  public static function send_request($url, $post_params=NULL, $options=NULL, $cookie=NULL, &$ch=NULL) {
    // global $config;
    if ($ch == NULL) {
      echo "curl_init<br/>";
      $ch = curl_init();
    }
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);

    // curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0; en-US) Gecko/20100101 Firefox/12.0');
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    // if ($options) {
      // curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
      // curl_setopt($ch, CURLOPT_PROXYPORT, $options['port']);
    // }
    if ($options) {
      // $mproxy = Array();
      // $pproxy = "/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/";
      // preg_match($pproxy,$proxy,$mproxy);
      // $IP = $mproxy[0];
      // curl_setopt($ch, CURLOPT_PROXY, $IP);
      // curl_setopt($ch, CURLOPT_PROXYPORT, 60099);
      curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
      curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
      curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
      curl_setopt($ch, CURLOPT_PROXYPORT, $options['port']);
      // curl_setopt($ch, CURLOPT_PROXYUSERPWD, "nbphuoc:dihana1604");
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

    //set the header params
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";
    //assign to the curl request.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($post_params) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, count($post_params));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
    }
    $result = curl_exec($ch);
    if ($result) {
      return $result;
    } else {
      return false;
    }
    curl_close($ch);
  }

  public static function http_code($ch) {
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
  }

  public static function cURLc($url, $head = false, $cookie = null, $p=NULL) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, $head);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    //curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0; en-US) Gecko/20100101 Firefox/12.0');

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);
    //set the header params
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";
    //assign to the curl request.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($p) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $p);
    }
    $result = curl_exec($ch);
    if ($result) {
      return $result;
    } else {
      return curl_error($ch);
    }
    curl_close($ch);
    //set the header params
  }

  public static function send_request_location($url, $post_params=NULL, $options=NULL, $cookie=NULL, &$ch=NULL) {
    // global $config;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);

    // curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0; en-US) Gecko/20100101 Firefox/12.0');
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    // if ($options) {
      // curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
      // curl_setopt($ch, CURLOPT_PROXYPORT, $options['port']);
    // }
    if ($options) {
      // $mproxy = Array();
      // $pproxy = "/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/";
      // preg_match($pproxy,$proxy,$mproxy);
      // $IP = $mproxy[0];
      // curl_setopt($ch, CURLOPT_PROXY, $IP);
      // curl_setopt($ch, CURLOPT_PROXYPORT, 60099);
      echo "CHANGE PROXY : " . $options["proxy"] . " " . $options["port"] . "<br/>";
      curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
      curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
      curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
      curl_setopt($ch, CURLOPT_PROXYPORT, $options['port']);
      // curl_setopt($ch, CURLOPT_PROXYUSERPWD, "nbphuoc:dihana1604");
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    //set the header params
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";
    //assign to the curl request.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($post_params) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, count($post_params));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
    }
    $result = curl_exec($ch);
    if ($result) {
      return $result;
    } else {
      return false;
    }
    curl_close($ch);
  }
  public static function send_request2($ch, $url, $post_params=NULL, $options=NULL) {
    // global $config;
    // $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    //curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0; en-US) Gecko/20100101 Firefox/12.0');
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    if ($options) {
      curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
      curl_setopt($ch, CURLOPT_PROXYPORT, $options['port']);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    //set the header params
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";
    //assign to the curl request.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($post_params) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, count($post_params));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
    }
    $result = curl_exec($ch);
    if ($result) {
      return $result;
    } else {
      return false;
    }
    curl_close($ch);
  }
  public static function escape_JS( $stringLiteral, $mode ) {
    if ($mode != ESCAPE_MODE_SINGLE) {
      $mode = ESCAPE_MODE_DOUBLE;
    }

    switch ( $mode ) {
    case ESCAPE_MODE_DOUBLE:
      $searches = array( '"', "\n" );
      $replacements = array( '\\"', "\\n\"\n\t+\"" );
      break;
    case ESCAPE_MODE_SINGLE:
      $searches = array( "'", "\n" );
      $replacements = array( "\\'", "\\n'\n\t+'" );
      break;
    }
    return str_replace( $searches, $replacements, $stringLiteral );
  }

  public static function file_url_contents($url){
    $crl = curl_init();
    $timeout = 30;
    curl_setopt ($crl, CURLOPT_URL,$url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    curl_close($crl);
    return $ret;
  } //file_url_contents ENDS

  public static function remove_html_tags($str) {
    $str = preg_replace(
      array(// Remove invisible content
        '@<head[^>]*?>.*?</head>@siu',
        '@<noscript[^>]*?.*?</noscript>@siu',
      ),
      "", //replace above with nothing
      $str );
    return $str;
  }

  public static function getRealIpAddr() {
    echo "HTTP_CLIENT_IP: " . ($_SERVER['HTTP_CLIENT_IP']) . "<br/>";
    echo "HTTP_X_FORWARDED_FOR: " . ($_SERVER['HTTP_X_FORWARDED_FOR']) . "<br/>";
    echo "REMOTE_ADDR: " . ($_SERVER['REMOTE_ADDR']) . "<br/>";
    echo "HTTP_REFERER: " . ($_SERVER['HTTP_REFERER']) . "<br/>";
    echo "==================</br>";

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }

  public static function check_proxy($ip, $port) {
    if($con = @fsockopen($ip, $port, $eroare, $eroare_str, 3)) {
      fclose($con);
      return true;
    } else {
      print "fail fsockopen $ip:$port" . '<br>';
      return false;
    }
  }

  public static function check_proxy_by_access_url($ip, $port) {
    // $result = Util::send_request("http://tainguyenweb.com/util/ip.php", NULL,
      // array("proxy" => $ip, "port" => $port));

    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://tainguyenweb.com/util/ip.php");
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    curl_setopt($ch, CURLOPT_PROXY, $ip);
    curl_setopt($ch, CURLOPT_PROXYPORT, $port);

    $result = curl_exec($ch);
    if ($result) {
      return true;
    }

    return false;
  }

  public static function shorten_str($str, $max_chars=15) {
    if (strlen($str) > $max_chars) {
      $str = substr($str, 0, $max_chars);
    }
    return $str;
  }

  public static function get_value_from_html($name_input, $html_content) {
    $regex = "/<input.*?name=\"$name_input\".*?value=\"(\w+)\"/";
    preg_match($regex, $html_content, $match);
    if (count($match) > 0) {
      return $match[1];
    }
    return "";
  }

  public static function inStr($s,$as){
    $s=strtoupper($s);
    if(!is_array($as)) $as=array($as);
    for($i=0;$i<count($as);$i++) if(strpos(($s),strtoupper($as[$i]))!==false) return true;
    return false;
  }

  // returns a word from the variables array:
  private static function get_word($key, $r_vars){
    if (isset($r_vars[$key])){

      $words = $r_vars[$key];

      // calc max.
      $w_max = count($words)-1;
      $w_rand = rand(0, $w_max);

      // return the word, and check if the word contains
      // another variable:
      return Util::replace_words(trim($words[$w_rand]), $r_vars);
    }
    else {
      // the word was not found :-(
      return "(Error: Word '$key' was not found!)";
    }
  }

  // this function replaces a variable like %something with
  // the 	proper variable-value:
  private static function replace_words($sentence, $r_vars){
    // if there are no variables in the sentence,
    // return it without doing anything
    if (str_replace('%', '', $sentence) == $sentence)
      return $sentence;

    // split the words up:
    $words = explode(" ", $sentence);

    $new_sentence = array();

    // go trough all words:
    for($w=0; $w < count($words); $w++){

      $word = trim($words[$w]);

      if ($word != ''){

        // is this word a variable?
        if (preg_match('/^%(.*)$/', $word, $m)){

          // --> yes
          $varkey = trim($m[1]);

          // get the proper word from the variable list:
          $new_sentence[] = Util::get_word($varkey, $r_vars);
        }
        else {
          // --> no it is a default word
          $new_sentence[] = $word;
        }

      }

    }

    // join the array to a new sentence:
    return implode(" ", $new_sentence);
  }

  public static function random_sentence($name) {

    //
    // A list of sentences:
    //
    // %something ==> is a variable
    //
    $r_sentences = '
This is a %adjective %noun , %random_name %sentence_ending
%random_name %sentence_ending This is another %noun %noun %sentence_ending
%random_name %sentence_ending I %love_or_hate %nouns , because it is %adjective %sentence_ending
My %family says you, %random_name , are not %adjective %sentence_ending
These %nouns are %adjective , %random_name %sentence_ending
      ';

    //
    // This is another list of variables:
    // (This list can also contain variables (like %something))
    //
    // Formatting:
    // (first-line) = Variablename
    // (second-line) = Variables (seperated by semicolon)
    //

    $r_variables = '
adjective
%adjective_list;very %adjective_list;deadly %adjective_list

adjective_list
big;huge;small;red;blue;cool;yellow;pink;fluffy;stupid;clever;fat;lazy;boring

noun
%noun_list;%adjective %noun_list;%nouns

noun_list
sentence;beer;cow;monkey;donkey;example;ice cream;dog

nouns
beers;monkeys;donkeys;examples;cars;trees;birds;dogs;aardvark;addax;alligator;alpaca;anteater;antelope;aoudad;ape;argali;armadillo;ass;baboon;badger;basilisk;bat;bear;beaver;bighorn;bison;boar;budgerigar;buffalo;bull;bunny;burro;camel;canary;capybara;cat;chameleon;chamois;cheetah;chimpanzee;chinchilla;chipmunk;civet;coati;colt;cony;cougar;cow;coyote;crocodile;crow;deer;dingo;doe;dog;donkey;dormouse;dromedary;duckbill;dugong;eland;elephant;elk;ermine;ewe;fawn;ferret;finch;fish;fox;frog;gazelle;gemsbok;gila monster;giraffe;gnu;goat;gopher;gorilla;grizzly bear;ground hog;guanaco;guinea pig;hamster;hare;hartebeest;hedgehog;hippopotamus;hog;horse;hyena;ibex;iguana;impala;jackal;jaguar;jerboa;kangaroo;kid;kinkajou;kitten;koala;koodoo;lamb;lemur;leopard;lion;lizard;llama;lovebird;lynx;mandrill;mare;marmoset;marten;mink;mole;mongoose;monkey;moose;mountain goat;mouse;mule;musk deer;musk-ox;muskrat;mustang;mynah bird;newt;ocelot;okapi;opossum;orangutan;oryx;otter;ox;panda;panther;parakeet;parrot;peccary;pig;platypus;polar bear;pony;porcupine;porpoise;prairie dog;pronghorn;puma;puppy;quagga;rabbit;raccoon;ram;rat;reindeer;reptile;rhinoceros;roebuck;salamander;seal;sheep;shrew;silver fox;skunk;sloth;snake;springbok;squirrel;stallion;steer;tapir;tiger;toad;turtle;vicuna;walrus;warthog;waterbuck;weasel;whale;wildcat;wolf;wolverine;wombat;woodchuck;yak;zebra;zebu

love_or_hate
love;hate;like

family
%adjective %family_members;%family_members

family_members
grandpa;brother;sister;mom;dad;grandma

sentence_ending
.;!;!!;!?;*lol*;!;@;#
';

    // strip spaces:
    $r_sentences = trim($r_sentences);
    $r_variables = trim($r_variables);

    $r_variables = $r_variables . "\n\nrandom_name\n$name";

    // fix new lines and split sentences up:
    $r_sentences = str_replace("\r\n", "\n", $r_sentences);
    $r_sentences = str_replace("\r", "\n", $r_sentences);
    $r_sentences = explode("\n", $r_sentences);

    $r_variables = str_replace("\r\n", "\n", $r_variables);
    $r_variables = str_replace("\r", "\n", $r_variables);
    $r_variables = explode("\n\n", $r_variables);

    // this array contains all variables:
    $r_vars = array();

    // go trough all variables:
    for($x=0; $x < count($r_variables); $x++){
      $var = explode("\n", trim($r_variables[$x]));
      // lowecase all:
      $key = strtolower(trim($var[0]));
      // split words:
      $words = explode(";", trim($var[1]));
      // add variables to the $r_vars Array
      $r_vars[$key] = $words;
    }

    // calc. max.
    $max_s = count($r_sentences)-1;
    $rand_s = rand(0, $max_s);

    // get a random sentence:
    $sentence = $r_sentences[$rand_s];

    // format the resulting sentence, so that I looks nice:
    // (delete whitespace infront of punctuation marks)
    $sentence = str_replace(' ,', ',', ucfirst(Util::replace_words($sentence, $r_vars)));
    $sentence = str_replace(' .', '.', $sentence);
    $sentence = str_replace(' !', '!', $sentence);
    $sentence = str_replace(' ?', '?', $sentence);
    $sentence = trim($sentence);

    // finally print the new sentence! :-D
    return $sentence;
  }

  public static function check_proxy_curl_shell($ip, $port) {
    $output = shell_exec("curl -x $ip:$port http://tainguyenweb.com/util/ip.php --connect-timeout 15");
    echo $output;

    if (strpos($output, "SUCCESS") == 0) {
      return true;
    }
    return false;
  }

  public static function get_proxy_obj() {
    require_once "database.php";
    require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

    $db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
    $con = $db->setDB();

    // Change IP
    $i = 0;
    $ip = "";
    $port = "";
    while (true) {
      $result = $db->makeQuery("SELECT * FROM `proxies` ORDER BY RAND() LIMIT 0,1;");
      $data = $db->fetchItem($result);
      $tmp_ip = $data["ip"];
      $tmp_port = intval($data["port"]);
      $retry_count = $data["retry_count"];
      if (!$tmp_ip) {
        break;
      }
      $i++;
      if ($i > 20) break;

      // $registered = $db->get_rows("`old_ips` WHERE ip='$tmp_ip' AND port='$tmp_port';");
      // if (Util::check_proxy_curl_shell($tmp_ip, $tmp_port)) {
      if (Util::check_proxy($tmp_ip, $tmp_port) && Util::check_proxy_by_access_url($tmp_ip, $tmp_port)) {
        echo "OK1: " . $tmp_ip . " " . $tmp_port . "<br/>";
        $ip = $tmp_ip;
        $port = $tmp_port;
        break;
      } else {
        echo "FAIL: $tmp_ip $tmp_port<br/>";
        $db->makeQuery("DELETE from `proxies` where ip='$tmp_ip' and port='$tmp_port';");
        $current_time = time();
        $current_time = date('Y-m-d H:i:s', $current_time);
        $db->makeQuery("insert into old_proxies(ip, port, created_at) values('$tmp_ip', '$tmp_port', '$current_time')");
        continue;
      }
    } // end while

    $proxy_obj = NULL;
    if ($ip && $port) {
      $proxy_obj = array("proxy" => $ip, "port" => $port);
    }

    return $proxy_obj;
  }
}
?>
