cd /var/www
mkdir project
cd project
git clone git@bitbucket.org:tamvominh/im-5r.git
cd /var/www
mkdir im
cd im
chmod -R 777 /var/www/project/im-5r/
ln -s /var/www/project/im-5r/config .
ln -s /var/www/project/im-5r/inc .
ln -s /var/www/project/im-5r/util .
ln -s /var/www/project/im-5r/twitter .
ln -s /var/www/project/im-5r/php.ini .
cd /var/www/im/
- Mysql
mysql -u root
create database youtube;
use youtube;
mysql -u root -p youtube < /var/www/im/util/schema/ddl.sql
mysql -u root youtube < twitter/schema/ddl.sql
- Read README.md in util/hma/README.md
  + Install phantomjs
wget http://phantomjs.googlecode.com/files/phantomjs-1.6.1-linux-x86_64-dynamic.tar.bz2
tar jxf phantomjs-1.6.1-linux-x86_64-dynamic.tar.bz2
ln -s /var/www/phantomjs-1.6.1-linux-x86_64-dynamic/bin/phantomjs /usr/bin/phantomjs
# apt-get install phantomjs ~> NOT WORK

- Change config
cp config/config.php.example config/config.php
vim im/config/config.php


- Runing get ips
curl http://localhost/im/util/get_ips_all.php -> Get Ips from freeproxylist, hma, coolproxy
curl http://localhost/im/util/reg_acc_wo_captcha.php -> Register Twitter account
curl http://localhost/im/util/link_from_mail.php -> Read mail and get confirmation link
curl http://localhost/im/util/confirm_mail.php -> Read confirmation link from db and follow link

- Optional:
curl http://localhost/im/util/bot_post_status.php -> Post random status
curl http://localhost/im/util/bot_follow.php -> Follow followers

