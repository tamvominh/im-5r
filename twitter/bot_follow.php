<?php
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "minibots.class.php";

function followUser($username, $password, $follower, $proxy_obj) {
  if (!function_exists("curl_init")) die("twitterSetStatus needs CURL module, please install CURL on your php.");
  $ch = curl_init();
		// get login form and parse it
  $cookie_file = "." . DIRECTORY_SEPARATOR . "cookies" . DIRECTORY_SEPARATOR . "$username.txt";
  if (file_exists($cookie_file)) {
    unlink($cookie_file);
  }

  curl_setopt($ch, CURLOPT_URL, "https://mobile.twitter.com/session/new");
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_FAILONERROR, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 5);
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3 ");
  if ($proxy_obj) {
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    curl_setopt($ch, CURLOPT_PROXY, $proxy_obj['proxy']);
    curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_obj['port']);
  }

  $page = curl_exec($ch);
  $page = Util::remove_html_tags($page);
  // echo "Login page: $page <br/>";

  // $page = stristr($page, "<div id>");
  preg_match("/form action=\"(.*?)\"/", $page, $action);
  $regex = "/<input.+?name=\"authenticity_token\".+?value=\"(\w+)\"/";
  preg_match($regex, $page, $authenticity_token);
  // echo "Auth token: $authenticity_token[1]";

  // -------------------------------------------------------
  // make login and get home page
  $strpost = "authenticity_token=".urlencode($authenticity_token[1])."&username=".urlencode($username)."&password=".urlencode($password);
  curl_setopt($ch, CURLOPT_URL, $action[1]);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $strpost);
  $page = curl_exec($ch);
  $page = Util::remove_html_tags($page);
  // echo $page;

  // $follower_page = Util::send_request("https://mobile.twitter.com/$follower", NULL, $proxy_obj);
  // $follower_page = Util::remove_html_tags($follower_page);

  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
  curl_setopt($ch, CURLOPT_URL, "https://mobile.twitter.com/$follower");
  $follower_page = curl_exec($ch);
  // echo $follower_page;

  $regex = "/<input.+?name=\"authenticity_token\".+?value=\"(\w+)\"/";
  preg_match($regex, $page, $authenticity_token);
  if (count($authenticity_token) == 0) {
    return false;
  }

  echo "Authen for follower page: $authenticity_token[1] <br/>";
  $strpost = "authenticity_token=".urlencode($authenticity_token[1])."&commit=Follow";
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
  curl_setopt($ch, CURLOPT_URL, "https://mobile.twitter.com/$follower/follow");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $strpost);

  $after_follow_page = curl_exec($ch);
  $after_follow_page = Util::remove_html_tags($after_follow_page);
  // echo $after_follow_page;
  return true;
}

// ==========================================
$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();
$bot = new Minibots();

$result = $db->makeQuery("select * from followers where friends_count <= target_count and suspend = false order by rand() limit 0, 1;");
$data = $db->fetchItem($result);
$follower_id = $data["id"];
$follower = $data["username"];
$target_count = intval($data["target_count"]);
$friends_count = intval($data["friends_count"]);
if (!$follower_id) {
  echo "No Followers!!<br/>";
  exit(1);
}
echo "FOLLOWER: $follower , Friends count: " . $friends_count . "<br/>";

// Get info of followers
$twitterInfo = $bot->twitterInfoApi($follower);
if ($twitterInfo) {
  $followers = intval($twitterInfo["followers"]);
  $db->makeQuery("update followers set friends_count = $followers where username = '$follower';");
} else {
  echo "$follower suspended 1\n";
  // $db->makeQuery("update followers set suspend = true where username = '$follower';");
  exit(0);
}

// for ($i = $friends_count; $i <= ($target_count + 1); $i++) {
$i = $friends_count;
while (true) {
  // Get info of username/password
  $query = "select * from twitter_users where suspend = false and `twitter_users`.id not in (select `user_followers`.twitter_user_id from user_followers where `user_followers`.follower_id = $follower_id) order by rand() limit 0, 1;";
  echo "$query <br/>";
  $result = $db->makeQuery($query);
  $data = $db->fetchItem($result);

  $username = $data["username"];
  $password = $data["password"];
  $twitter_user_id = $data["id"];
  // $username = "tamvominh22";
  // $password = "reborn";

  // $twitterInfo = $bot->twitterInfoApi($username);
  // if (!$twitterInfo) {
  // echo "$username suspended\n";
  // $db->makeQuery("update twitter_users set suspend = true where username = '$username';");
  // exit(0);
  // }

  // $proxy_obj = NULL;
  $proxy_obj = Util::get_proxy_obj();

  // Follow
  echo "Username: $username <br/>";
  echo "Password: $password <br/>";
  echo "Follower: $follower <br/>";
  echo "=============<br/>";
  $result = followUser($username, $password, $follower, $proxy_obj);
  if ($result) {
    $i++;
    $db->makeQuery("insert into user_followers(twitter_user_id, follower_id) values($twitter_user_id, $follower_id);");
    if ($i >= ($target_count + 1)) {
      $twitterInfo = $bot->twitterInfoApi($follower);
      if ($twitterInfo) {
        $followers = intval($twitterInfo["followers"]);
        $db->makeQuery("update followers set friends_count = $followers where username = '$follower';");
        $i = $followers;

        if ($i >= ($target_count + 1)) {
          echo "DONE!! $follower with $target_count <br/>";
          exit(0);
        }
      } else {
        echo "$follower suspended 2\n";
        $db->makeQuery("update followers set suspend = true where username = '$follower';");
        exit(0);
      }
    }
  }
}
