<?php
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "minibots.class.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();
$bot = new Minibots();

$proxy_obj = Util::get_proxy_obj();

$result = $db->makeQuery("select * from twitter_users where suspend = false and validate = true order by updated_at asc limit 0, 1;");
$data = $db->fetchItem($result);

$username = $data["username"];
$password = $data["password"];

$twitterInfo = $bot->twitterInfoApi($username);
if (!$twitterInfo) {
  echo "$username suspended\n";
  $db->makeQuery("update twitter_users set suspend = true where username = '$username';");
  exit(0);
}
$date = date('Y-m-d H:i:s', time());
$db->makeQuery("update twitter_users set updated_at = '$date' where username = '$username';");

$result = $db->makeQuery("select * from fakenames order by rand() limit 0, 1;");
$data = $db->fetchItem($result);
$name = $data["givename"] . " " . $data["surname"];

$sentence = Util::random_sentence($name);
$result = $bot->twitterSetStatus($username, $password, $sentence, $proxy_obj);

if ($result) {
  echo "Post status for user $username/$password with: $sentence\n<br/>";
} else {
  echo "Post status for user $username/$password fail";
}
?>
