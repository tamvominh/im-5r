<?php
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

$file_cookie = "." . DIRECTORY_SEPARATOR . "cookies" . DIRECTORY_SEPARATOR;

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

$proxy_obj = NULL;
$proxy_obj = Util::get_proxy_obj();

$result = $db->makeQuery("select * from twitter_users where link is not null and validate = false limit 0, 1;");
$data = $db->fetchItem($result);

$username = $data["username"];
if (count($username) == 0) {
  exit(0);
}
$link = $data["link"];
$password = $data["password"];

echo "Username: $username<br/>";
echo "Link: $link<br/>";

$regex = "/confirm_email\/(\w+)\/([\w-]+)\?/";
preg_match($regex, $link, $matches);
$confirmation_key = $matches[2];
echo "Confirmation key: $confirmation_key<br/>";

// $uid_regex = "/uid=\d+&iid=[-\w]+&nid=[\w+-]+/";
$uid_regex = "/uid=.*?$/";
preg_match($uid_regex, $link, $match_uid);
$uid_suffix = $match_uid[0];
echo "uid suffix: $uid_suffix <br/>";

// $count = $db->get_rows("twitter_users WHERE username='$username';");
// if ($count == 0) {
  // $db->insert("twitter_users", array("username", "password", "email", "validate"), array($username, "HappyDay", $email, true));
// } else {
// $db->edit("twitter_users", "validate", 1, "username", $username);
// }

echo "url " . $link;
$file_cookie = $file_cookie . "$username.txt";
if (file_exists($file_cookie)) {
  unlink($file_cookie);
}

$ch = curl_init();
$response = Util::send_request($link, NULL, $proxy_obj, $file_cookie, $ch);
$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
// echo $http_code;

if ($http_code == 302 || $http_code == 301) {
  $regex = "/<a href=\"(.*?)\">/";
  echo "Response:=$response=";
  preg_match($regex, $response, $match);

  $redirect_url = $match[1];
  echo "- Redir: $redirect_url<br/>";

  $response = Util::send_request($redirect_url, NULL, $proxy_obj, $file_cookie, $ch);
  $regex = "<input type=\"hidden\" value=\"(\w+)\" name=\"authenticity_token\"/>";
  preg_match($regex, $response, $match);
  $authenticity_token = $match[1];

  echo "<br>=====$response</br>";
  echo "Authen: $authenticity_token";

  // if (strpos($response, "Your timeline is currently empty")) {
    // continue;
  // }

  $params = array(
    'session[username_or_email]' => $username,
    'session[password]' => $password,
    'scribe_log' => "",
    'redirect_after_login' => '/account/confirm_email/' . $username . '/' . $confirmation_key . "?" . $uid_suffix,
    'authenticity_token' => $authenticity_token,
    'remember_me' => 1
  );

  // echo "PARAMS: ";
  // print_r($params);
  // echo "==<br/>";

  // $file_cookie = "./cookies/$username.txt";
  // session%5Busername_or_email%5D=StephenEHo_164&session%5Bpassword%5D=HappyDay&authenticity_token=1458d44806a3bac011dbaea18f11d506bd19e99e&scribe_log=&redirect_after_login=%2Faccount%2Fconfirm_email%2FStephenEHo_164%2F38H6A-85A68-134427%3Fuid%3D741178440%26iid%3Dam-99344911113442781663812977%26nid%3D18%2B309&authenticity_token=1458d44806a3bac011dbaea18f11d506bd19e99e
  // $ch = NULL;
  // $response_login = Util::send_request_location("https://twitter.com/sessions", $params, array("proxy" => $ip, "port" => $port),
  $response_login = Util::send_request("https://twitter.com/sessions", $params, $proxy_obj, $file_cookie, $ch);
  echo "<br/>===========After LOGIN: <br/>";
  echo $response_login;

  $regex = "/<a href=\"(.*?)\">/";
  preg_match($regex, $response_login, $match);
  $redirect_url = $match[1];
  echo "- After login: $redirect_url <br/>";
  // $ch = NULL;
  // $response = Util::send_request_location($redirect_url, NULL, array("proxy" => $ip, "port" => $port), $file_cookie, $ch);
  $response = Util::send_request($redirect_url, NULL, $proxy_obj, $file_cookie, $ch);
  $db->edit("twitter_users", "validate", 1, "username", $username);
  // echo "After redirec: " . $response . "=";

  // if (strpos($response, "Your timeline is currently empty")) {
    // echo "SUCCESSFUL for $username <br/>";
    // imap_delete($inbox,$uid, FT_UID);
  // }
} else {
  echo "Can't access URL";
}
