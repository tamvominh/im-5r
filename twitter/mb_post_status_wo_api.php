<?php
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

$username = "tamvominh22";
$password = "reborn";

$cookie_file = "./cookies/$username.txt";
if (file_exists($cookie_file)) {
  unlink($cookie_file);
}

$ip = "";
$port = "";
$proxy_obj = NULL;
if ($ip && $port) {
  $proxy_obj = array("proxy" => $ip, "port" => $port);
}

$signin_page = Util::send_request("https://mobile.twitter.com/session/new", NULL,
  $proxy_obj, $cookie_file);
// echo $signin_page;

// DELETE
$file = fopen("pages/signin_page.html", "w") or die("Can't open file");
fwrite($file, $signin_page);
fclose($file);

$regex = "/<input name=\"authenticity_token\".+?value=\"(\w+)\"/";
preg_match($regex, $signin_page, $match);
$authenticity_token = $match[1];

echo "Authenticity_token for signpage: $authenticity_token<br/>";
$params = array(
  "authenticity_token" => $authenticity_token,
  "username" => $username,
  "password" => $password,
  "commit" => "Sign+in"
);

$ch = NULL;
$after_signin_page = Util::send_request("https://mobile.twitter.com/session", $params,
  $proxy_obj, $cookie_file, $ch);
echo "After signin page: $after_signin_page <br/>";

$http_code = Util::http_code($ch);
echo "HTTP Code after_signin_page: $http_code <br/>";

if ($http_code != 302) {
  echo "Login failed<br/>";
  exit(0);
}

$regex = "/<a href=\"(.*?)\">/";
preg_match($regex, $after_signin_page, $match);
$redirect_url = $match[1];
echo "- Redir: $redirect_url;";

$ch = NULL;
$main_page = Util::send_request($redirect_url, NULL, $proxy_obj, $cookie_file, $ch);
echo "Main page: $main_page<br/>";

//=========== Post status
$ch = NULL;
$compose_page = Util::send_request("https://mobile.twitter.com/compose/tweet", NULL,
  $proxy_obj, $cookie_file, $ch);
echo "Compose page: $compose_page <br/>";

// DELETE
$file = fopen("pages/compose_page.html", "w") or die("Can't open file");
fwrite($file, $compose_page);
fclose($file);

$regex = "/<input name=\"authenticity_token\".+?value=\"(\w+)\"/";
preg_match($regex, $compose_page, $match);
$authenticity_token = $match[1];
echo "Authenticity_token for compose page: $authenticity_token<br/>";

$params = array(
  "authenticity_token" => $authenticity_token,
  "tweet[text]" => "Sample tweet"
);

$ch = NULL;
$after_compose_page = Util::send_request("https://mobile.twitter.com/", $params,
  $proxy_obj, $cookie_file, $ch);
echo $after_compose_page;

$http_code = Util::http_code($ch);
echo "HTTP Code after_compose_page: $http_code <br/>";
?>
