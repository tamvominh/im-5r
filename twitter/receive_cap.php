<?php
error_reporting(0);

require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "deathbycaptcha.php";

require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

define(DBC_USERNAME, "hantrang");
define(DBC_PASSWORD, "hnamart");

function sendParamsAgainWithRecaptchaResponse($answer) {
  $ip = $_POST["ip"];
  $port = $_POST["port"];

  $proxy = NULL;
  if ($ip) {
    $proxy = array("proxy" => $ip, "port" => $port);
  }
  $username = $_POST["user"]["screen_name"];

  $params = array(
    'ad_id' => "",
    'ad_ref' => "",
    'user[name]' => $_POST["user"]["name"],
    'user[email]' => $_POST["user"]["email"],
    'user[user_password]' => $_POST["user"]["user_password"],
    'user[screen_name]' => $_POST["user"]["screen_name"],
    'user[discoverable_by_email]' => $_POST["user"]["discoverable_by_email"],
    'user[remember_me_on_signup]' => $_POST["user"]["remember_me_on_signup"],
    'user[send_email_newsletter]' => $_POST["user"]["send_email_newsletter"],
    'user[use_cookie_personalization]' => $_POST["user"]["use_cookie_personalization"],
    'asked_cookie_personalization_setting' => $_POST["asked_cookie_personalization_setting"],
    'authenticity_token' => $_POST["authenticity_token"],
    'recaptcha_challenge_field' => $_POST["recaptcha_challenge_field"],
    'recaptcha_response_field' => $answer
  );

  $cookie_file = "./cookies/$username.txt";
  $ch = NULL;
  $result_twitter = Util::send_request("https://twitter.com/account/create", $params,
    $proxy, $cookie_file, $ch);

  echo $result_twitter;
}

$client = new DeathByCaptcha_SocketClient(DBC_USERNAME, DBC_PASSWORD);
$client->is_verbose = true;

$url = $_POST["url"];
// $url = "http://www.google.com/recaptcha/api/image?c=03AHJ_VutnIdCPnpzApZb62lVtqnjlaggvhL8Q6oRecxI3vXmMW8rp26RfH46ZjHU7JITTY_vjjtlPTAfPMa8klHKKh2_tSdhSMpOr_A0IPu0YZ_1DJ5TwtgiicANLzJCmUzWDgFn66aG9j4vTxKUWBCdqVKIIHEjLog";

if ($captcha = $client->upload($url)) {
  sleep(10);
  if ($text = $client->get_text($captcha['captcha'])) {
    echo $text;
    sendParamsAgainWithRecaptchaResponse($text);
    // Report an incorrectly solved CAPTCHA.
    //$client->report($captcha['captcha']);
  } else {
    sleep(20);
    if ($text = $client->get_text($captcha['captcha'])) {
      echo $text;
      sendParamsAgainWithRecaptchaResponse($text);
    } else {
      echo "NO_RESULT";
    }
  }
}

?>
