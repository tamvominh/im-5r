<?php
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

// Change IP
$i = 0;
$ip = "";
$port = "";

// while ($i <= 20) {
while (true) {
  $result = $db->makeQuery("SELECT * FROM `proxies` ORDER BY RAND() LIMIT 0,1;");
  $data = $db->fetchItem($result);
  $tmp_ip = $data["ip"];
  $tmp_port = $data["port"];
  $retry_count = $data["retry_count"];
  if (!$tmp_ip) {
    break;
  }
  $i++;

  $registered = $db->get_rows("`old_ips` WHERE ip='$tmp_ip' AND port='$tmp_port';");
  if ($registered == 0 && Util::check_proxy($tmp_ip, $tmp_port)) {
    echo "OK1: " . $tmp_ip . " " . $tmp_port . "<br/>";
    if (Util::check_proxy_by_access_url($tmp_ip, $tmp_port)) {
      echo "OK2: " . $tmp_ip . " " . $tmp_port . "<br/>";
      $ip = $data["ip"];
      $port = $data["port"];
      break;
    } else {
      echo "failed with check_proxy_by_access_url";
      if ($retry_count < 10) {
        $db->makeQuery("UPDATE proxies set retry_count=" . ($retry_count + 1));
      } else {
        $db->makeQuery("DELETE from `proxies` where ip='$tmp_ip' and port='$tmp_port';");
      }

      continue;
    }
    // $content = Util::send_request("http://tainguyenweb.com/youtube/print_ip.php", NULL,
      // array("proxy" => $data["ip"], "port" => $data["port"]));
  } else {
    if ($registerd == 0) {
      $db->insert("old_ips", array("ip", "port"), array($data["ip"], $data["port"]));
    } else {
      $db->makeQuery("DELETE from `proxies` where ip='$tmp_ip' and port='$tmp_port';");
    }
    echo "FAIL: " . $data["ip"] . " " . $data["port"] . "<br/>";
    continue;
  }
}

if (!$ip) {
  echo "Can't connect any IPs";
  exit(0);
}

$content = Util::send_request("https://twitter.com/signup", NULL, NULL, NULL, $ch);
$regex = "<input type=\"hidden\" value=\"(\w+)\" name=\"authenticity_token\"/>";
preg_match($regex, $content, $match);
$authenticity_token = $match[1];

// Get fake name
$result = $db->makeQuery("SELECT * FROM `fakenames` ORDER BY RAND() LIMIT 0,1;");
$data = $db->fetchItem($result);
$name = Util::shorten_str($data["givenname"] . " " . $data["surname"], 20);
// $password = $data["password"];
$password = "HappyDay";
$email_parts = preg_split("/@/", $data["emailaddress"]);
$username = Util::shorten_str($email_parts[0], 10) . "_" . substr(time(), 7);
$email =  $username . "@hostfreevietnam.com";
// echo $name . " " . $password . " " . $email;

// print_r($db->fetchItem($result));

// $name = "tamvo_" . substr(time(), 3);
// $email = $name . "@hostfreevietnam.com";

$params = array(
  'user[name]' => $name,
  'user[email]' => $email,
  'user[user_password]' => "HappyDay",
  'user[screen_name]' => $username,
  'user[discoverable_by_email]' => 1,
  'user[remember_me_on_signup]' => 1,
  'user[send_email_newsletter]' => 1,
  'user[use_cookie_personalization]' => 0,
  'asked_cookie_personalization_setting' => 0,
  'authenticity_token' => $authenticity_token
);

// $params

// echo Util::send_request("https://twitter.com/signup");
$ch = NULL;

// $cookie_file = "/Users/tamvo/code/im/php/twitter/cookies/" . $username . ".txt";
$cookie_file = "./cookies/$username.txt";
$result_twitter = Util::send_request("https://twitter.com/account/create", $params,
array("proxy" => $ip, "port" => $port), $cookie_file, $ch);
// Your account has been confirmed
// $result_twitter = "";

$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
echo "HTTPCode: $http_code <br/>";

if ($http_code == 302) {
  // Register succcessfully
  $current_time = time();
  $db->insert("twitter_users", array("username", "password", "name", "email", "created_at", "updated_at"),
    array($username, $password, $name, $email, date('Y-m-d H:i:s', $current_time), date('Y-m-d H:i:s', $current_time)));

  // $db->insert("registered_ips", array("ip", "port"), array($ip, $port));
  echo "SUCCESS";
} else if ($http_code == 0) {
  echo "FAIL";
  $db->makeQuery("DELETE from `old_ips` where ip='$ip' and port='$port';");
} else if (strpos($result_twitter, "please try again later") != -1) {
  // $db->insert("registered_ips", array("ip", "port"), array($ip, $port));
  echo "You can't do that right now.";
} else {
  echo $result_twitter;
  $file_name = "result_twitter.html";
  $file = fopen($file_name, "w") or die("Can't open extract.js file");
  fwrite($file, $result_twitter);
  fclose($file);
?>
  <script language="javascript" type="text/javascript">
function loadScript(url, callback) {
    // adding the script tag to the head as suggested before
   var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;

   // then bind the event to the callback function
   // there are several events for cross browser compatibility
   script.onreadystatechange = callback;
   script.onload = callback;

   // fire the loading
   head.appendChild(script);
}

loadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js", function() {
  $(function() {
    setTimeout(function() {
      var recaptcha_url = $("#recaptcha_image img").attr("src");
      var recaptcha_challenge_field = $("#recaptcha_challenge_field_holder").find("input[name=recaptcha_challenge_field]").val();

      $.ajax({
        url: "/im/twitter/receive_cap.php",
        type: "POST",
        data: {
          'url': recaptcha_url,
          'ip': '<?php echo $ip; ?>',
          'port': '<?php echo $port; ?>',

          'user[name]': '<?php echo $name; ?>',
          'user[email]': '<?php echo $email; ?>',
          'user[user_password]': "HappyDay",
          'user[screen_name]': '<?php echo $username; ?>',
          'user[discoverable_by_email]': 1,
          'user[remember_me_on_signup]': 1,
          'user[send_email_newsletter]': 1,
          'user[use_cookie_personalization]': 1,
          'asked_cookie_personalization_setting': 1,
          'recaptcha_challenge_field': recaptcha_challenge_field
        },
        success: function(response) {
        }
      });
    }, 10000);
  });

});
  </script>

<?php
}

$db->insert("old_ips", array("ip", "port"), array($ip, $port));
$db->closeConnection($con);
?>
