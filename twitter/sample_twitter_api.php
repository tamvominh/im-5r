<?php
session_start();

require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "util.php";
require_once ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
require_once ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";

include 'lib/EpiCurl.php';
include 'lib/EpiOAuth.php';
include 'lib/EpiTwitter.php';
include 'lib/secret.php';

$twitterObj = new EpiTwitter($consumer_key, $consumer_secret);
$oauth_token = $_GET['oauth_token'];
$url = $twitterObj->getAuthorizationUrl();

echo "authorization url: $url <br>";

$ch = NULL;
$response_authorization = Util::send_request($url, NULL, NULL, NULL, $ch);

echo "HTTP code: " . Util::http_code($ch) . "<br/>";
echo $response_authorization;

$authenticity_token = Util::get_value_from_html("authenticity_token", $response_authorization);
$oauth_token = Util::get_value_from_html("oauth_token", $response_authorization);

echo "authenticity_token: $authenticity_token <br/>";
echo "oauth_token: $oauth_token <br/>";

// authenticity_token=002121d81ad6f6ea4251c38027a7e2ceacd2bae4&oauth_token=13HBVHBFhQRhu5DM6DFj0Iy1orvii9mhlwoq0qXg&session%5Busername_or_email%5D=tamvominh22&session%5Bpassword%5D=reborn&remember_me=1
$params = array(
  'authenticity_token' => $authenticity_token,
  'oauth_token' => $oauth_token,
  'session[username_or_email]' => "tamvominh22",
  'session[password]' => "reborn"
);

$ch = NULL;
$cookie_file = "./cookies/$username.txt";
$resp_after_authorize = Util::send_request("https://twitter.com/oauth/authorize", $params,
  NULL, $cookie_file, $ch);

echo "HTTP code: " . Util::http_code($ch) . "<br/>";
// echo $resp_after_authorize;

$regex = "/<meta.*?oauth_token=(\w+)&oauth_verifier=(\w+)/";
preg_match($regex, $resp_after_authorize, $match);
$oauth_token = $match[1];
$oauth_verifier = $match[2];
echo "oauth_token: $oauth_token <br/>";
echo "oauth_verifier: $oauth_verifier <br/>";

$twitterObj->setToken($oauth_token);
$token = $twitterObj->getAccessToken();
$twitterObj->setToken($token->oauth_token, $token->oauth_token_secret);
$twitterInfo= $twitterObj->get_accountVerify_credentials();
$twitterInfo->response;

$username = $twitterInfo->screen_name;
$profilepic = $twitterInfo->profile_image_url;

echo "username: $username <br/>";
echo "profilepic: $profilepic <br/>";

$update_status = $twitterObj->post_statusesUpdate(array('status' => "1From post_status haaha"));
$temp = $update_status->response;

$json_str = json_encode($update_status);
echo "update_status: $json_str <br/>";
echo "temp: $temp <br/>";

$resp = $twitterObj->get('/statuses/followers.json');
echo "num of follower: " . count($resp);
// echo "followers: $resp";

$resp = $twitterObj->get('/statuses/friends.json');
echo "num of friends: " . count($resp);
// echo "friends: $resp";

echo "Friend Ids: ";
// $twitterFriends=$twitterObj->get_friendsIds(array('screen_name' => 'RaymondPGa_746'));
// $twitterFriends->response;
// echo $twitterFriends->responseText;
// foreach($twitterFriends as $friend) { echo $friend->id; }
$resp = $twitterObj->get_usersShow(array('screen_name' => 'RaymondPGa_746'));
$json_str = json_encode($resp);
echo "User show: $json_str";

//TODO
// $resp = $twitterObj->post("/friendships/create.json", array("id" => "745097418", "oauth" => $oauth_token));
$resp = $twitterObj->post_friendshipsCreate(array("id" => "745097418", "oauth" => $oauth_token));
$json_str = json_encode($resp);
echo "response create relationship: $json_str";
