create table registered_ips (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11)
);

create table old_ips (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11)
);

drop table if exists twitter_users;
create table twitter_users (
  id int(11) primary key auto_increment,
  username varchar(255),
  password varchar(255),
  name varchar(255),
  email varchar(255),
  created_at datetime,
  updated_at datetime,
  validate TINYINT default false
);
alter table twitter_users add column validate TINYINT default false;
alter table twitter_users add column suspend tinyint default 0;
alter table twitter_users add column link varchar(255);
alter table twitter_users add column status varchar(255);

create index username_idx on twitter_users(username);
create index username_password_idx on twitter_users(username, password);

create index index_old_ips on old_ips(ip, port);
create index index_proxies on proxies(ip, port);

drop table if exists followers;
create table followers (
  id int(11) primary key auto_increment,
  username varchar(255),
  friends_count int(11) default 0,
  target_count int(11) default 0,
  suspend tinyint default 0,
  created_at datetime,
  updated_at datetime
);

create table user_followers (
  twitter_user_id int(11),
  follower_id int(11)
);
