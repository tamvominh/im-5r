<?php
class Twitter {
  function email_available($email) {
    $result = Util::send_request("https://twitter.com/users/email_available",
      array("email" => $email));
    $json_data = json_decode($result);

    return $json_data->{"valid"};
  }

  function username_available($username) {
    $result = Util::send_request("https://twitter.com/users/username_available",
      array("email" => $username));
    $json_data = json_decode($result);

    return $json_data->{"valid"};
  }
}
?>
