* WAY 1: Must open in Browser (from_hma.php)
  + Steps:
    - GET: Get content from HMA
    - Using jQuery to remove hidden field, then submit back again
    - POST: Parse to get IPs, then import into DB

* WAY 2: Can run in background
  + Require: phantomjs is install
  + Steps:
    - GET: Get content from HMA
    - Duplicate extract_template.js to extract.js with content from HMA
    - In this JS, append to sample page test_js.html (Because the element must be visible)
    - Using jQuery to remove hidden field, then submit back again
    - POST: Parse to get IPs, then import into DB
  + Install:
    - Install phantomjs (headless webkit javascript)
    - Change config_hma.php
    - Deploy to <HOST>/im/util/hma/. Otherwise, should change extract_template.js
    - Open extract_template.js, to specify the HOST
    - (Optional) Change number of HMA pages to fetch (by default: 4 pages ~ 200 IPs): from_hma_2.php
