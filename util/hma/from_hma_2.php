<?php
define( "ESCAPE_MODE_DOUBLE", 1 );
define( "ESCAPE_MODE_SINGLE", 2 );

function escape_JS( $stringLiteral, $mode ) {
  if ($mode != ESCAPE_MODE_SINGLE) {
    $mode = ESCAPE_MODE_DOUBLE;
  }

  switch ( $mode ) {
  case ESCAPE_MODE_DOUBLE:
    $searches = array( '"', "\n" );
    $replacements = array( '\\"', "\\n\"\n\t+\"" );
    break;
  case ESCAPE_MODE_SINGLE:
    $searches = array( "'", "\n" );
    $replacements = array( "\\'", "\\n'\n\t+'" );
    break;
  }
  return str_replace( $searches, $replacements, $stringLiteral );
}

function file_url_contents($url){
  $crl = curl_init();
  $timeout = 30;
  curl_setopt ($crl, CURLOPT_URL,$url);
  curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
  $ret = curl_exec($crl);
  curl_close($crl);
  return $ret;
} //file_url_contents ENDS

function remove_html_tags($str) {
  $str = preg_replace(
    array(// Remove invisible content
      '@<head[^>]*?>.*?</head>@siu',
      '@<noscript[^>]*?.*?</noscript>@siu',
    ),
    "", //replace above with nothing
    $str );
  return $str;
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  require_once "config_hma.php";

  define(HMA_URL, "http://hidemyass.com/proxy-list/");
  // echo remove_html_tags(file_url_contents(HMA_URL));

  $template_file_name = "extract_template.js";
  $file_template = fopen($template_file_name, "r");
  $js_template = fread($file_template, filesize($template_file_name));
  fclose($file_template);

  // Get content from HMA
  $html_content = remove_html_tags(file_url_contents(HMA_URL));

  // TODO: The number of proxies want to get
  for ($i=2; $i<=4; $i++) {
    $html_content = $html_content . remove_html_tags(file_url_contents(HMA_URL . $i));
  }
  // echo $html_content;

  $js_src = "extract.js";
  $file = fopen($js_src, "w") or die("Can't open extract.js file");
  $html = escape_JS($html_content, ESCAPE_MODE_DOUBLE);
  fwrite($file, str_replace("__CHANGE_HERE__", $html, $js_template));
  fclose($file);

  // echo remove_html_tags(file_url_contents(HMA_URL));
  // echo $html;
  // echo "<br/>==============================================================<br/>";

  $output = shell_exec(PHANTOMJS_PATH . ' extract.js');
  // echo $output;
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  // require_once $_SERVER['DOCUMENT_ROOT'] . "/inc/database.php";
  // require_once $_SERVER['DOCUMENT_ROOT'] . "/config/config.php";
  require_once ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "inc" . DIRECTORY_SEPARATOR . "database.php";
  require_once ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";
  $db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
  $con = $db->setDB();

  $content = $_POST["content"];
  // $db->makeQuery("TRUNCATE hma_proxies;");
  // $db->makeQuery("DELETE FROM hma_proxies;");

  $regex = "/\b((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\b\s+(\d+)/";
  preg_match_all($regex, $content, $matches);

  for ($i=1; $i < count($matches[1]); $i++) {
    // $db->insert("hma_proxies", array("ip", "port"), array($matches[1][$i], $matches[2][$i]));
    $db->insert("proxies", array("ip", "port", "source"), array($matches[1][$i], $matches[2][$i], "hma"));
    // echo $matches[1][$i] . " " . $matches[2][$i] . "\n";
  }
  $db->closeConnection($con);
}
?>
