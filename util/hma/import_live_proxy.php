<?php
include "../inc/database.php";
include "../config/config.php";

function check_proxy($ip, $port) {
  if($con = @fsockopen($ip, $port, $eroare, $eroare_str, 3))
  {
    // print "GOOD $ip:$port" . '<br>';
    fclose($con); // Close the socket handle
    return true;
  } else {
    print "BAD $ip:$port" . '<br>';
    return false;
  }
}

// check_proxy("195.222.35.110", "3128");
// fclose($fisier);

if ($_FILES['file']['error'] != UPLOAD_ERR_OK || !is_uploaded_file($_FILES['file']['tmp_name'])) {
  die("Upload failed");
}

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

$file_content = file_get_contents($_FILES['file']['tmp_name']);
$ips = explode("\n", $file_content);

//TODO: Testing
$db->makeQuery("DELETE FROM proxies;");
foreach ($ips as $proxy) {
  $proxy = trim($proxy);
  if (!$proxy) {
    continue;
  }
  $data = explode(":", $proxy);
  $ip = $data[0];
  $port = $data[1];

  $result = check_proxy($ip, $port);
  if ($result) {
    if ($db->get_rows("proxies WHERE ip='$ip' AND port=$port;") == 0) {
      $db->insert('proxies', array('ip', 'port'), array($ip, $port));
    } else {
      echo "Duplicate: $ip:$port" . "<br/>";
    }
  }
}
$db->closeConnection($con);
?>
