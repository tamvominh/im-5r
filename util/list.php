<?php
// if (empty($_GET)) {
  // $_GET = array();
// }
?>
<html>
  <head>
    <form action="list.php" method="get">
      <label for="table_name">Filename:</label>
      <select name="table_name">
      <option value="client_ips" <?php echo $_GET["table_name"] == "client_ips" ? "selected" : "" ?>> client_ips </option>
      <option value="proxies" <?php echo $_GET["table_name"] == "proxies" ? "selected" : "" ?>> proxies </option>
      <option value="old_proxies" <?php echo $_GET["table_name"] == "old_proxies" ? "selected" : "" ?>> old_proxies </option>
      <option value="hma_proxies" <?php echo $_GET["table_name"] == "hma_proxies" ? "selected" : "" ?>> hma_proxies </option>
      <option value="registered_ips" <?php echo $_GET["table_name"] == "registered_ips" ? "selected" : "" ?>> registered_ips </option>
      <option value="twitter_users" <?php echo $_GET["table_name"] == "twitter_users" ? "selected" : "" ?>> twitter_users </option>
      <option value="old_ips" <?php echo $_GET["table_name"] == "old_ips" ? "selected" : "" ?>> old_ips </option>
      </select>
      <br /> <br />
      <input type="submit" name="submit" value="Submit" />
    </form>
  </head>
  <body>
  </body>

</html>

<?php
include "../inc/database.php";
include "../config/config.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

$table_name = $_GET["table_name"];
if (!$table_name) {
  $table_name = "client_ips";
}

$result = $db->makeQuery("select * from `$table_name`;");
$field_names = array();
$i = 0;
while ($i < mysql_num_fields($result)) {
  $meta = mysql_fetch_field($result, $i);

  $field_names[] = $meta->name;
  $i++;
}
$db->closeConnection($con);
?>

<table border="1">
  <tr>
    <th>#</th>
    <?php
      foreach ($field_names as $field) {
        echo "<th>" . ucwords($field) . "</th>";
      }
    ?>
  </tr>
<?php
$i = 1;
while ($row = mysql_fetch_assoc($result)) {
  echo '<tr>';
  echo '<td>' . $i++ . '</td>';
  for($j=0; $j < count($row); $j++) {
    echo '<td>' . $row[$field_names[$j]] . '</td>';
  }
  echo '</tr>';
}
?>
</table>

