create table proxies (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11),
  source varchar(255),
  retry_count int(11) default 0
);

create table old_proxies (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11),
  created_at datetime
);
