<?php
// include "inc/database.php";
// include "config/config.php";

/* By Grant Burton @ BURTONTECH.COM (11-30-2008): IP-Proxy-Cluster Fix */
function checkIP($ip) {
  if (!empty($ip) && ip2long($ip)!=-1 && ip2long($ip)!=false) {
    $private_ips = array (
      array('0.0.0.0','2.255.255.255'),
      array('10.0.0.0','10.255.255.255'),
      array('127.0.0.0','127.255.255.255'),
      array('169.254.0.0','169.254.255.255'),
      array('172.16.0.0','172.31.255.255'),
      array('192.0.2.0','192.0.2.255'),
      array('192.168.0.0','192.168.255.255'),
      array('255.255.255.0','255.255.255.255')
    );

    foreach ($private_ips as $r) {
      $min = ip2long($r[0]);
      $max = ip2long($r[1]);
      if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max)) return false;
    }
    return true;
  } else {
    return false;
  }
}

function determineIP() {
  if (checkIP($_SERVER["HTTP_CLIENT_IP"])) {
    return $_SERVER["HTTP_CLIENT_IP"];
  }
  foreach (explode(",",$_SERVER["HTTP_X_FORWARDED_FOR"]) as $ip) {
    if (checkIP(trim($ip))) {
      return $ip;
    }
  }
  if (checkIP($_SERVER["HTTP_X_FORWARDED"])) {
    return $_SERVER["HTTP_X_FORWARDED"];
  } elseif (checkIP($_SERVER["HTTP_X_CLUSTER_CLIENT_IP"])) {
    return $_SERVER["HTTP_X_CLUSTER_CLIENT_IP"];
  } elseif (checkIP($_SERVER["HTTP_FORWARDED_FOR"])) {
    return $_SERVER["HTTP_FORWARDED_FOR"];
  } elseif (checkIP($_SERVER["HTTP_FORWARDED"])) {
    return $_SERVER["HTTP_FORWARDED"];
  } else {
    return $_SERVER["REMOTE_ADDR"];
  }
}

function getRealIpAddr() {
  // echo "HTTP_CLIENT_IP: " . ($_SERVER['HTTP_CLIENT_IP']) . "<br/>";
  // echo "HTTP_X_FORWARDED_FOR: " . ($_SERVER['HTTP_X_FORWARDED_FOR']) . "<br/>";
  // echo "REMOTE_ADDR: " . ($_SERVER['REMOTE_ADDR']) . "<br/>";
  // echo "HTTP_REFERER: " . ($_SERVER['HTTP_REFERER']) . "<br/>";
  // echo "==================</br>";

  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}
//Override server variable for WordPress comments
// $_SERVER["REMOTE_ADDR"] = determineIP();
// echo(determineIP());
echo "SUCCESS: " . getRealIpAddr();

// print_r($_SERVER);
// echo ($_SERVER['REMOTE_PORT']);
// $client_ip = getRealIpAddr();
// echo $client_ip;

// $db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
// $con = $db->setDB();

// $result = $db->makeQuery("SELECT * FROM client_ips WHERE ip='$client_ip';");

// if (mysql_num_rows($result) == 0) {
// $db->insert('client_ips', array('ip', 'count'), array($client_ip, 1));
// } else {
// while ($row = $db->fetchAssoc($result)) {
// $count = $row["count"];
// $db->edit('client_ips', "count", $count+1, "ip", $client_ip);
// }
// }

// $db->closeConnection($con);
?>
