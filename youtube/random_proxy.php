<?php
include "inc/database.php";
include "config/config.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

$result = $db->makeQuery("SELECT * FROM `proxies` ORDER BY RAND() LIMIT 0,1;");

$data = $db->fetchItem($result);
echo $data["ip"] . ":" . $data["port"];
?>
