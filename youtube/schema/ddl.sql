drop table proxies;
create table proxies (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11)
);

drop table client_ips;
create table client_ips (
  id int(11) primary key auto_increment,
  client_ip varchar(255),
  remote_addr varchar(255),
  x_forwarded_for varchar(255),
  referer varchar(255),
  user_agent varchar(255),
  port int(11),
  count int(11) default 0
);

drop table hma_proxies;
create table hma_proxies (
  id int(11) primary key AUTO_INCREMENT,
  ip varchar(255),
  port int(11)
);
