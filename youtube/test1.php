<?php
/* get column metadata */
include "inc/database.php";
include "config/config.php";

$db = new DBConnect(constant("DB_HOST"), constant("DB_NAME"), constant("DB_USERNAME"), constant("DB_PASSWORD"));
$con = $db->setDB();

// mysql_select_db('youtube');

$result = mysql_query('select * from client_ips;');
if (!$result) {
  die('Query failed: ' . mysql_error());
}

$i = 0;
while ($i < mysql_num_fields($result)) {
  echo "Information for column $i:<br />\n";
  $meta = mysql_fetch_field($result, $i);
  if (!$meta) {
    echo "No information available<br />\n";
  }
  echo "<pre>
    blob:         $meta->blob
    max_length:   $meta->max_length
    multiple_key: $meta->multiple_key
    name:         $meta->name
    not_null:     $meta->not_null
    numeric:      $meta->numeric
    primary_key:  $meta->primary_key
    table:        $meta->table
    type:         $meta->type
    unique_key:   $meta->unique_key
    unsigned:     $meta->unsigned
    zerofill:     $meta->zerofill
    </pre>";
  $i++;
}
?>

